<?php
/*
Plugin Name: Cookie Disclamer
Plugin URI:  #
Description: Simple Cookie Disclamer plugin
Version:     0.1
Author:      Haranin Andrii
Author URI:  http://garanin.in.ua
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: cookie-disclamer
Domain Path: /languages
*/

defined( 'ABSPATH' ) or die( 'No!' );

// Mobile Detect Class
require_once 'public/assets/mobile_detect/Mobile_Detect.php';

// Initiate admin settings page
require_once 'admin/cookieDisclaimerAdminInit.php';

// Initiate plugin settings
require_once 'admin/cookieDisclaimerSettings.php';

// Initiate plugin front enqueues
$detect = new Mobile_Detect;

if( get_option( 'cookie_disclaimer_enabled' ) && ( !$detect->isMobile() || $detect->isTablet() ) )
	require_once 'public/cookieDisclaimerPopup.php';
