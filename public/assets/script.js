jQuery(function($){

    /**
     * Show cookie disclaimer if no cookie found
     */
    $(document).ready(function () {
        var links = $('#cookie_disclaimer .disclaimer_content').find('a');
        links.each(function(){
            // Check if target attribute exists and set _blank if not. If attribute exists than someone placed it specially.
            !$(this).prop('target') ? $(this).prop('target','_blank') : null;
        })


        // Check if cookie cookie_disclaimer_closed not exist and show disclaimer
        if( !cookie.read('cookie_disclaimer_closed') ) {
            $('#cookie_disclaimer').slideDown();
        }
    })

    /**
     * Hide cookie disclaimer and create cookie
     */
    $(document).on('click','.close_cookie_disclaimer',function () {

        // Hide disclaimer on close or accept click
        $(this).parent('#cookie_disclaimer').slideUp();

        // Create cookie_disclaimer_closed cookie flag
        cookie.create('cookie_disclaimer_closed', true);
    })

    /**
     * Cookie manager
     * @type {{create: createCookie, read: readCookie}}
     */
    var cookie = {
        create: function createCookie(name, value, days) {
            if (days) {
                var date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                var expires = "; expires=" + date.toGMTString();
            }
            else var expires = "";

            document.cookie = name + "=" + value + expires + "; path=/";
        },
        read: function readCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1, c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        }
    }
});