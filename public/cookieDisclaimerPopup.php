<?php
class cookieDisclaimerPopup {
	function __construct() {

		add_action( 'wp_footer', array($this, 'embedCookieDisclaimerHTML') );
		add_action( 'wp_enqueue_scripts', array($this, 'addCookieDisclaimerStylesAndScript') );

		$this->addCookieDisclaimerHTML();
		$this->addCookieDisclaimerStylesAndScript();
	}

	/**
	 * Embed Cookie Disclaimer HTML at footer
	 */
	public function embedCookieDisclaimerHTML()
	{
		echo $this->addCookieDisclaimerHTML();
	}

	/**
	 * Generate Cookie Disclaimer HTML
	 */
	public function addCookieDisclaimerHTML()
	{
		$button_text = esc_html(get_option('cookie_disclaimer_button') );
		$html = '';

		$html .= '<div id="cookie_disclaimer"><span class="close_cookie_disclaimer">&#10005;</span><div class="disclaimer_content">';
		$html .= wpautop(get_option('cookie_disclaimer_copy'));
		$html .= '</div><button title="'.$button_text.'" class="close_cookie_disclaimer">'.$button_text.'</button></div>';

		return $html;
	}

	/**
	 * Add Cookie Disclaimer styles and scripts
	 */
	public function addCookieDisclaimerStylesAndScript()
	{
		wp_enqueue_style('cookie_disclaimer_style', plugins_url( '/assets/style.css', __FILE__ ),array(),null);
		wp_enqueue_script('cookie_disclaimer_script', plugins_url( '/assets/script.js', __FILE__ ),array('jquery'),null,true);
		if ( is_rtl() ) wp_enqueue_style('cookie_disclaimer_style', plugins_url( '/assets/style-rtl.css', __FILE__ ),array(),null);
	}
}

new cookieDisclaimerPopup();