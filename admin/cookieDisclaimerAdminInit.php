<?php
class cookieDisclaimerAdminInit
{
	function __construct() {
		add_action(
			'admin_menu',
			array(
				$this,
				'cookie_disclaimer_options_page'
			)
		);
	}

	/**
	 * Options page settings form
	 */
	public function cookie_disclaimer_options_page_html() {

		// check user capabilities
		if (!current_user_can('manage_options')) {
			return;
		}
		?>

		<div class="wrap">
			<?php settings_errors(); ?>

			<form id="cookie_disclaimer_form" action="options.php" method="post">
				<?php
				settings_fields( 'cookie_disclaimer_enable_section' );
				do_settings_sections( 'cookie_disclaimer' );

				submit_button('Save Settings');
				?>
			</form>
		</div>
		<?php
	}

	/**
	 * Add Cookie Disclaimer menu item at Tools section
	 */
	function cookie_disclaimer_options_page()
	{
		add_submenu_page(
			'tools.php',
			'Cookie Disclamer Options',
			'Cookie Disclamer Options',
			'manage_options',
			'cookie_disclaimer',
			array($this, 'cookie_disclaimer_options_page_html')
		);
	}
}

new cookieDisclaimerAdminInit();