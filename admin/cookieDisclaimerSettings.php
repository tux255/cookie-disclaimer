<?php

/**
 * Created by PhpStorm.
 * User: haranin
 * Date: 02.09.17
 * Time: 19:12
 */
class cookieDisclaimerSettings {
	function __construct() {
		add_action('admin_init', array($this, 'cookie_disclaimer_settings_init'));
	}

	/**
	 * Initialise plugin settings
	 */
	public function cookie_disclaimer_settings_init()
	{
		add_settings_section(
			'cookie_disclaimer_enable_section',
			'Cookie Disclaimer Settings',
			array(
				$this,
				'cookie_disclaimer_callback'
			),
			'cookie_disclaimer'
		);

		// Enable/disable cookie disclaimer popup
		add_settings_field(
			'cookie_disclaimer_enabled',
			__('Enable Cookie Disclaimer'),
			array(
				$this,
				'cookie_disclaimer_enabled_callback'
			),
			'cookie_disclaimer',
			'cookie_disclaimer_enable_section',
			array(
				'option_name' => 'cookie_disclaimer_enabled'
			)
		);
		register_setting( 'cookie_disclaimer_enable_section', 'cookie_disclaimer_enabled');

		// Main content copy
		add_settings_field(
			'cookie_disclaimer_copy',
			__('Main content text'),
			array(
				$this,
				'cookie_disclaimer_copy_callback'
			),
			'cookie_disclaimer',
			'cookie_disclaimer_enable_section',
			array(
				'option_name' => 'cookie_disclaimer_copy'
			)
		);
		register_setting( 'cookie_disclaimer_enable_section', 'cookie_disclaimer_copy');
		// Button copy
		add_settings_field(
			'cookie_disclaitmer_button',
			__('Accept button text'),
			array(
				$this,
				'cookie_disclaimer_button_callback'
			),
			'cookie_disclaimer',
			'cookie_disclaimer_enable_section',
			array(
				'option_name' => 'cookie_disclaimer_button'
			)
		);
		register_setting( 'cookie_disclaimer_enable_section', 'cookie_disclaimer_button');
	}
	/**
     * Create checkbox input
	 * @param $val
	 */
	public function cookie_disclaimer_enabled_callback( $val ) {
		$option_name = $val['option_name'];
		?>
            <input
                type="checkbox"
                name="<?php echo $option_name ?>"
                value="true"
                <?php echo get_option($option_name) ? 'checked="checked"' : ''; ?>
            />
		<?php
	}
	/**
     * Create WYSIWYG text field
	 * @param $val
	 */
	public function cookie_disclaimer_copy_callback ( $val ) {
		$option_name = $val['option_name'];

		$settings = array(
			'teeny' => true,
			'textarea_rows' => 15,
			'tabindex' => 1
		);
		wp_editor(
		        get_option('cookie_disclaimer_copy'),
                $option_name,
                $settings
        );
	}
	/**
	 * Create WYSIWYG text field
	 * @param $val
	 */
	public function cookie_disclaimer_button_callback ( $val ) {
		$option_name = $val['option_name'];
		?>
        <input
                type="text"
                name="<?php echo $option_name ?>"
                value="<?php echo get_option($option_name) ?>"
        />
		<?php
	}

	/**
	 * Required callback
	 */
	public function cookie_disclaimer_callback() {}
}

new cookieDisclaimerSettings();